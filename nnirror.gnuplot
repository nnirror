u(x) = -log(1/x-1)
set autoscale
set xtics 1000
set ytics 2
set mxtics 10
set mytics 2
set grid xtics
set grid mxtics
set grid ytics
set grid mytics
unset key
set term pngcairo enhanced size 10080,7200 font "LMSans10,100" linewidth 3
set output "out.png"
set multiplot layout 4,1 title "becoming self-aware: training a neural network to recognize itself"
set title "self-recognition"
set key bottom right maxrows 1 font ",75"
plot [0:9999][0:10] for [i=0:9] sprintf("%d.log", i) u 1:(u(column(2))) w l lc rgbcolor hsv2rgb((i+1.5)/10,1,1) dt 1 t sprintf("%d", i)
set title "self-other demarcation"
set key bottom right maxrows 1 font ",75"
plot [0:9999][0:10] for [i=0:9] sprintf("%d.log", i) u 1:(-u(column(3))) w l lc rgbcolor hsv2rgb((i+1.5)/10,1,1) dt 1 t sprintf("%d", i)
set title "pareidolia"
set key top right maxrows 1 font ",75"
plot [0:9999][-10:0] for [i=0:9] sprintf("%d.log", i) u 1:(u(column(4))) w l lc rgbcolor hsv2rgb((i+1.5)/10,1,1) dt 1 t sprintf("%d", i)
set title "score"
set key bottom right maxrows 1 font ",75"
plot [0:9999][0:10] for [i=0:9] sprintf("%d.log", i) u 1:((2*u(column(2))-u(column(3))-u(column(4)))/4) w l lc rgbcolor hsv2rgb((i+1.5)/10,1,1) dt 1 t sprintf("%d", i)
unset multiplot
