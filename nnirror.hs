{-
  nnirror (c) 2018,2019 Claude Heiland-Allen
  A neural network looking at itself.

  Copyleft: This is a free work, you can copy, distribute, and
  modify it under the terms of the Free Art License
  <http://artlibre.org/licence/lal/en/>.
-}

{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE NoStarIsType #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

import Grenade
import Codec.Picture (readImage, DynamicImage(ImageRGBA8), Image(Image))
import Control.Concurrent.Async (async, wait)
import Control.Exception (catch, SomeException)
import Control.Monad (forM_, replicateM, unless, when)
import Control.Monad.Random (MonadRandom, randomRIO)
import Data.Either (lefts, rights)
import Data.IORef (newIORef, modifyIORef, readIORef)
import Data.List (sort, sortBy, transpose)
import Data.Ord (comparing)
import Data.Proxy (Proxy(Proxy))
import Data.Serialize (Serialize(..), runGet, runPut)
import Data.Singletons
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as V
import Data.Word (Word8)
import Foreign (allocaBytes, castPtr, nullPtr, peek, peekArray, plusPtr, sizeOf, with, withArray)
import Foreign.C.String (withCString)
import GHC.TypeLits
import Graphics.GL.Core33
import qualified Graphics.UI.GLFW as GLFW
import qualified Numeric.LinearAlgebra as LA
import Numeric.LinearAlgebra.Static as LAS hiding (C, (|||), (===), (<>))
import System.Environment (getArgs)
import System.Exit (exitFailure, exitSuccess)
import System.IO (hFlush, hPutBuf, hPutStr, hPutStrLn, stderr, stdout)

type C = 10
type F = 48

type Ego = Network
  '[ Pad 1 1 1 1, Convolution 3 C 3 3 1 1, Pooling 2 2 2 2, Logit
   , Pad 1 1 1 1, Convolution C C 3 3 1 1, Pooling 2 2 2 2, Logit
   , Pad 1 1 0 1, Convolution C C 3 3 1 1, Pooling 2 2 2 2, Logit
   , Pad 1 1 1 1, Convolution C C 3 3 1 1, Pooling 2 2 2 2, Logit
   , Reshape
   , FullyConnected (4 * 2 * C) F, Logit
   , FullyConnected F 1, Logit
   ]
  '[ 'D3 64 36 3
   , 'D3 66 38 3, 'D3 64 36 C, 'D3 32 18 C, 'D3 32 18 C
   , 'D3 34 20 C, 'D3 32 18 C, 'D3 16  9 C, 'D3 16  9 C
   , 'D3 18 10 C, 'D3 16  8 C, 'D3  8  4 C, 'D3  8  4 C
   , 'D3 10  6 C, 'D3  8  4 C, 'D3  4  2 C, 'D3  4  2 C
   , 'D1 (4 * 2 * C)
   , 'D1 F, 'D1 F
   , 'D1 1, 'D1 1
   ]

type Id = Network
  '[ FullyConnected F (4 * 2 * C), Logit
   , Reshape
   , Deconvolution C C 3 3 2 2, Logit
   , Pad 1 1 0 0
   , Convolution C C 3 3 1 1, Logit
   , Deconvolution C C 3 3 2 2, Logit
   , Pad 1 1 1 0
   , Convolution C C 3 3 1 1, Logit
   , Deconvolution C C 3 3 2 2, Logit
   , Pad 1 1 0 0
   , Convolution C C 3 3 1 1, Logit
   , Deconvolution C C 3 3 2 2, Logit
   , Pad 1 1 0 0
   , Convolution C 3 3 3 1 1, Logit
   ]
  '[ 'D1 F
   , 'D1 (4 * 2 * C), 'D1 (4 * 2 * C)
   , 'D3  4  2 C
   , 'D3  9  5 C, 'D3  9  5 C
   , 'D3 10  6 C
   , 'D3 8   4 C, 'D3  8  4 C
   , 'D3 17  9 C, 'D3 17  9 C
   , 'D3 18 11 C
   , 'D3 16  9 C, 'D3 16  9 C
   , 'D3 33 19 C, 'D3 33 19 C
   , 'D3 34 20 C
   , 'D3 32 18 C, 'D3 32 18 C
   , 'D3 65 37 C, 'D3 65 37 C
   , 'D3 66 38 C
   , 'D3 64 36 3, 'D3 64 36 3
   ]
-- deconvolution  outputRows ~ (inputRows - 1) * stride + kernelRows
-- convolution    outputRows ~ (inputRows - kernelRows) / stride + 1

reflE :: Ego -> S ('D3 64 36 3)
reflE = sigmoid . S3D . (\(Just j) -> j) . LAS.create . LA.reshape 36 . V.take (64 * 36 * 3) . (<> V.replicate (64 * 36 * 3) 0) . reflect

reflI :: Id -> S ('D3 64 55 3)
reflI = sigmoid . S3D . (\(Just j) -> j) . LAS.create . LA.reshape 55 . V.take (64 * 55 * 3) . (<> V.replicate (64 * 55 * 3) 0) . reflect

sigmoid x = 1 / (1 + exp (-x))
unsigmoid y = - log (1 / y - 1)

bashNaN x
  | isNaN x || isInfinite x = 0
  | otherwise = x

reiE :: S ('D3 64 36 3) -> Ego
reiE input@(S3D s) = case reify . map bashNaN . LA.toList . LA.flatten . extract . unsigmoid $ s of
  Just (ego, _) -> ego

reiI :: S ('D3 64 55 3) -> Id
reiI input@(S3D s) = case reify . map bashNaN . LA.toList . LA.flatten . extract . unsigmoid $ s of
  Just (id, _) -> id

gan :: (LearningParameters, LearningParameters) -> (Ego, Id) -> IO ((Ego, Id), [Double])
gan (learnE, learnI) (ego, id) = do
  noise <- randomOfShape
  poopE <- randomOfShape
  let up :: LearningParameters -> Gradients l -> Network l s -> Network l s
      up l grad net = applyUpdate l net grad

      upE = up learnE
      upI = up learnI

  a_realE <- async $ do
    let !realE             = reflE ego
    return realE
  a_fakeE <- async $ do
    let r@(!idT, !fakeE)   = {-# SCC "runNetwork/id" #-} (runNetwork id noise)
    return r
  realE <- wait a_realE
  (idT, fakeE) <- wait a_fakeE

      -- train Ego

  a_real <- async $ do
    let r@(!realTE, !realGE) = {-# SCC "runNetwork/ego/real"   #-} (runNetwork ego realE)
    return r
  a_fake <- async $ do
    let r@(!fakeTE, !fakeGE) = {-# SCC "runNetwork/ego/fake"   #-} (runNetwork ego fakeE)
    return r
  a_poop <- async $ do
    let r@(!poopTE, !poopGE) = {-# SCC "runNetwork/ego/poop"   #-} (runNetwork ego poopE)
    return r

  (realTE, realGE) <- wait a_real
  (fakeTE, fakeGE) <- wait a_fake
  (poopTE, poopGE) <- wait a_poop

  a_real'E <- async $ do
    let (!real'E, !_)      = {-# SCC "runGradient/ego/real1" #-} (runGradient ego realTE (realGE - 1))
    return real'E
  a_fake'E <- async $ do
    let (!fake'E, !_)      = {-# SCC "runGradient/ego/fake"  #-} (runGradient ego fakeTE  fakeGE)
    return fake'E
  a_poop'E <- async $ do
    let (!poop'E, !_)      = {-# SCC "runGradient/ego/poop"  #-} (runGradient ego poopTE  poopGE)
    return poop'E

  a_realPE <- async $ do
    let (!_, !realPE)      = {-# SCC "runGradient/ego/real"  #-} (runGradient ego realTE  realGE)
    return realPE
  a_fakePE <- async $ do
    let (!_, !fakePE)      = {-# SCC "runGradient/ego/fake1" #-} (runGradient ego fakeTE (fakeGE - 1))
    return fakePE
  a_poopPE <- async $ do
    let (!_, !poopPE)      = {-# SCC "runGradient/ego/poop1" #-} (runGradient ego poopTE (poopGE - 1))
    return poopPE

  a_real'I <- async $ do
    realPE <- wait a_realPE
    let (!real'I, !_)      = {-# SCC "runGradient/id/real" #-} (runGradient id  idT     realPE)
    return real'I
  a_fake'I <- async $ do
    fakePE <- wait a_fakePE
    let (!fake'I, !_)      = {-# SCC "runGradient/id/fake" #-} (runGradient id  idT     fakePE)
    return fake'I
  a_poop'I <- async $ do
    poopPE <- wait a_poopPE
    let (!poop'I, !_)      = {-# SCC "runGradient/id/poop" #-} (runGradient id  idT     poopPE)
    return poop'I

  a_ego <- async $ do
    real'E <- wait a_real'E
    fake'E <- wait a_fake'E
    poop'E <- wait a_poop'E
    let !newEgo = {-# SCC "update/ego" #-} (upE poop'E . upE fake'E . upE real'E $ ego)
    return newEgo

  a_id <- async $ do
    real'I <- wait a_real'I
    fake'I <- wait a_fake'I
    poop'I <- wait a_poop'I
    let !newId  = {-# SCC "update/id"  #-} (upI poop'I . upI fake'I . upI real'I $ id)
    return newId

  newEgo <- wait a_ego
  newId <- wait a_id
  let !real = fromSingleton realGE
      !fake = fromSingleton fakeGE
      !poop = fromSingleton poopGE

  return ((newEgo, newId), [real, fake, poop, (2 * unsigmoid real - unsigmoid fake - unsigmoid poop) / 4])

pixel :: Double -> Word8
pixel x = round $ 255 * clamp 0 1 x

clamp :: Ord a => a -> a -> a -> a
clamp lo hi x = lo `max` x `min` hi

{-
image :: Int -> Int -> Int -> [Double] -> Image
image h w c = take h . chunk w . chunk c . map pixel . (++ repeat 0)
-}
chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n xs = case splitAt n xs of (ys, zs) -> ys : chunk n zs
{-
type Image = [[[Word8]]]

(|||), (===) :: Image -> Image -> Image
(|||) = zipWith (++)
(===) = (++)

hcat, vcat :: [Image] -> Image
hcat = foldr (|||) (repeat [])
vcat = foldr (===) []
-}
toList :: S ('D3 w h c) -> [Double]
toList (S3D f) = LA.toList . LA.flatten . extract $ f

{-
display :: FilePath -> Ego -> Ego -> Ego -> IO ()
display file oldEgo ego newEgo = do
  let newEgoW = reflE newEgo
      egoW    = reflE ego
      oldEgoW = reflE oldEgo
      b = (7200 - 6912) `div` 2
      border = (replicate b 1 ++) . (++ replicate b 1)
      newDelta = 0.5 + 0.5 * signum (egoW - oldEgoW)
      oldDelta = 0.5 + 0.5 * signum (newEgoW - egoW)
      diff = 1 - abs (newDelta - oldDelta)
      row = B.pack . map pixel . border . toList $ diff
  B.appendFile file row
-}

recognition :: Ego -> S ('D3 64 36 3) -> Double
recognition ego = fromSingleton . snd . runNetwork ego

fromSingleton :: S ('D1 1) -> Double
fromSingleton (S1D s) = case LA.toList (extract s) of [t] -> t

{-
load :: Serialize a => FilePath -> IO a
load file = either fail return . runGet get =<< B.readFile file

save :: Serialize a => FilePath -> a -> IO ()
save file = B.writeFile file . runPut . put
-}

orElse :: IO a -> IO a -> IO a
orElse a e = a `catch` \(_::SomeException) -> e

randomEgoId :: IO (Ego, Id)
randomEgoId = do
  ego <- randomNetwork
  id <- randomNetwork
  return (ego, id)

randomLearning :: IO LearningParameters
randomLearning =
  LearningParameters <$> randomRIO (0.25, 0.75) <*> randomRIO (0.25, 0.75) <*> fmap (10**) (randomRIO (-16, 0))

--random :: IO (Ego, Id, LearningParameters, LearningParameters)
random = do
  (ego, id) <- randomEgoId
  learnE <- randomLearning
  learnI <- randomLearning
  return (ego, id, reflE ego, reflI id, learnE, learnI)

image :: S ('D3 w h 3) -> Vector Word8
image (S3D i) = normalize . LA.flatten . extract $ i

image' :: S ('D3 w h 3) -> Vector Word8
image' (S3D i) = V.map (pixel . (+ 0.5) . (/ 12) . unsigmoid) . LA.flatten . extract $ i

normalize xs =
  let n = V.length xs
      mi = V.minimum xs
      ma = V.maximum xs
      f x = pixel $ (x - mi) / (ma - mi)
  in  V.map f xs

main :: IO ()
main = do
      let width, height :: Num a => a
          width  = 1920
          height = 1080
      args <- getArgs
      let record = "record" `elem` args
      GLFW.setErrorCallback (Just $ \err msg -> hPutStrLn stderr msg >> exitFailure)
      True <- GLFW.init
      GLFW.windowHint $ GLFW.WindowHint'Resizable False
      GLFW.windowHint $ GLFW.WindowHint'ContextVersionMajor 3
      GLFW.windowHint $ GLFW.WindowHint'ContextVersionMinor 3
      GLFW.windowHint $ GLFW.WindowHint'OpenGLProfile GLFW.OpenGLProfile'Core
      GLFW.windowHint $ GLFW.WindowHint'OpenGLForwardCompat True
      Just window <- GLFW.createWindow width height "nnirror" Nothing Nothing
      GLFW.makeContextCurrent $ Just window
      --GLFW.swapInterval 2
      glViewport 0 0 width height
      glPixelStorei GL_PACK_ALIGNMENT 1
      glPixelStorei GL_UNPACK_ALIGNMENT 1

      tex <- withArray [0,0,0,0,0,0,0] $ \ptr -> glGenTextures 7 ptr >> peekArray 7 ptr
      glActiveTexture GL_TEXTURE0
      glBindTexture GL_TEXTURE_2D (tex !! 0)
      glTexImage2D GL_TEXTURE_2D 0 GL_RGB 36 64 0 GL_RGB GL_UNSIGNED_BYTE nullPtr
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_NEAREST
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_NEAREST
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE
      glActiveTexture GL_TEXTURE1
      glBindTexture GL_TEXTURE_2D (tex !! 1)
      glTexImage2D GL_TEXTURE_2D 0 GL_RGB 55 64 0 GL_RGB GL_UNSIGNED_BYTE nullPtr
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_NEAREST
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_NEAREST
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE
      glActiveTexture GL_TEXTURE2
      glBindTexture GL_TEXTURE_2D (tex !! 2)
      glTexImage2D GL_TEXTURE_2D 0 GL_RGB 36 64 0 GL_RGB GL_UNSIGNED_BYTE nullPtr
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_NEAREST
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_NEAREST
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE
      glActiveTexture GL_TEXTURE3
      glBindTexture GL_TEXTURE_2D (tex !! 3)
      glTexImage2D GL_TEXTURE_2D 0 GL_RGB 55 64 0 GL_RGB GL_UNSIGNED_BYTE nullPtr
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_NEAREST
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_NEAREST
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE
      glActiveTexture GL_TEXTURE4
      glBindTexture GL_TEXTURE_2D (tex !! 4)
      glTexImage2D GL_TEXTURE_2D 0 GL_RGB 143 1 0 GL_RGB GL_UNSIGNED_BYTE nullPtr
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_NEAREST
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_NEAREST
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE
      glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE
      glActiveTexture GL_TEXTURE5
      glBindTexture GL_TEXTURE_2D (tex !! 5)
      e <- readImage "training.png"
      case e of
        Right (ImageRGBA8 (Image w h dat)) -> V.unsafeWith dat $ \ptr -> do
          glTexImage2D GL_TEXTURE_2D 0 GL_RGBA (fromIntegral w) (fromIntegral h) 0 GL_RGBA GL_UNSIGNED_BYTE (castPtr ptr)
          glGenerateMipmap GL_TEXTURE_2D
          glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR_MIPMAP_LINEAR
          glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR
          glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE
          glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE
      glActiveTexture GL_TEXTURE6
      glBindTexture GL_TEXTURE_2D (tex !! 6)
      e <- readImage "scoreboard.png"
      case e of
        Right (ImageRGBA8 (Image w h dat)) -> V.unsafeWith dat $ \ptr -> do
          glTexImage2D GL_TEXTURE_2D 0 GL_RGBA (fromIntegral w) (fromIntegral h) 0 GL_RGBA GL_UNSIGNED_BYTE (castPtr ptr)
          glGenerateMipmap GL_TEXTURE_2D
          glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR_MIPMAP_LINEAR
          glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR
          glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE
          glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE

      frag <- glCreateShader GL_FRAGMENT_SHADER
      withCString (unlines
        [ "#version 330 core"
        , "uniform sampler2D dego, did, ego, id, score, mask;"
        , "uniform ivec2 size;"
        , "layout(location = 0) out vec4 cout;"
        , "void main() {"
        , "  vec4 m = texture(mask, vec2(0.0, 1.0) + vec2(1.0, -1.0) * gl_FragCoord.xy / vec2(size));"
        , "  vec4 c = vec4(vec3(0.0), 1.0);" -- texture(score, vec2(1.0, 0.5)).yxzw;"
        , "  vec2 t = gl_FragCoord.xy * vec2(254.0, 143.0) / vec2(size);"
        , "  if (5.0 <= t.x && t.x < 5.0 + 36.0 && 5.0 <= t.y && t.y < 5.0 + 64.0) { c = texture(dego, (t - vec2(5.0, 5.0)) / vec2(36.0, 64.0)); }"
        , "  if (5.0 + 36.0 + 5.0 <= t.x && t.x < 5.0 + 36.0 + 5.0 + 55.0 && 5.0 <= t.y && t.y < 5.0 + 64.0) { c = texture(did, (t - vec2(5.0 + 36.0 + 5.0, 5.0)) / vec2(55.0, 64.0)); }"
        , "  if (5.0 <= t.x && t.x < 5.0 + 36.0 && 5.0 + 64.0 + 5.0 <= t.y && t.y < 5.0 + 64.0 + 5.0 + 64.0) { c = texture(ego, (t - vec2(5.0, 5.0 + 64.0 + 5.0)) / vec2(36.0, 64.0)); }"
        , "  if (5.0 + 36.0 + 5.0 <= t.x && t.x < 5.0 + 36.0 + 5.0 + 55.0 && 5.0 + 64.0 + 5.0 <= t.y && t.y < 5.0 + 64.0 + 5.0 + 64.0) { c = texture(id, (t - vec2(5.0 + 36.0 + 5.0, 5.0 + 64.0 + 5.0)) / vec2(55.0, 64.0)); }"
        , "  if (5.0 + 36.0 + 5.0 + 55.0 + 5.0 <= t.x && t.x < 5.0 + 36.0 + 5.0 + 55.0 + 5.0 + 143.0 && 5.0 <= t.y && t.y < 5.0 + 41.0 ) { c = vec4(vec3((t.y - 5.0) / 41.0 < texture(score, vec2((t.x - (5.0 + 36.0 + 5.0 + 55.0 + 5.0))/143.0, 0.5))[2] ? 0.75 : 0.25), 1.0); }"
        , "  if (5.0 + 36.0 + 5.0 + 55.0 + 5.0 <= t.x && t.x < 5.0 + 36.0 + 5.0 + 55.0 + 5.0 + 143.0 && 5.0 + 41.0 + 5.0 <= t.y && t.y < 5.0 + 41.0 + 5.0 + 41.0) { c = vec4(vec3((t.y - (5.0 + 41.0 + 5.0)) / 41.0 < texture(score, vec2((t.x - (5.0 + 36.0 + 5.0 + 55.0 + 5.0))/143.0, 0.5))[1] ? 0.75 : 0.25), 1.0); }"
        , "  if (5.0 + 36.0 + 5.0 + 55.0 + 5.0 <= t.x && t.x < 5.0 + 36.0 + 5.0 + 55.0 + 5.0 + 143.0 && 5.0 + 41.0 + 5.0 + 41.0 + 5.0 <= t.y && t.y < 5.0 + 41.0 + 5.0 + 41.0 + 5.0 + 41.0) { c = vec4(vec3((t.y - (5.0 + 41.0 + 5.0 + 41.0 + 5.0)) / 41.0 < texture(score, vec2((t.x - (5.0 + 36.0 + 5.0 + 55.0 + 5.0))/143.0, 0.5))[0] ? 0.75 : 0.25), 1.0); }"
        , "  cout = mix(c, m, m.a);\n"
        , "}"
        ]) $ \s -> with s $ \string -> glShaderSource frag 1 string nullPtr
      glCompileShader frag
      vert <- glCreateShader GL_VERTEX_SHADER
      withCString (unlines
        [ "#version 330 core"
        , "void main() {"
        , "  if (gl_VertexID == 0) gl_Position = vec4(-1.0, -1.0, 0.0, 1.0);"
        , "  if (gl_VertexID == 1) gl_Position = vec4(-1.0,  1.0, 0.0, 1.0);"
        , "  if (gl_VertexID == 2) gl_Position = vec4( 1.0, -1.0, 0.0, 1.0);"
        , "  if (gl_VertexID == 3) gl_Position = vec4( 1.0,  1.0, 0.0, 1.0);"
        , "}"
        ]) $ \s -> with s $ \string -> glShaderSource vert 1 string nullPtr
      glCompileShader vert
      prog <- glCreateProgram
      glAttachShader prog vert
      glAttachShader prog frag
      glLinkProgram prog
      glUseProgram prog
      flip glUniform1i 0 =<< (withCString "dego" $ glGetUniformLocation prog)
      flip glUniform1i 1 =<< (withCString "did"  $ glGetUniformLocation prog)
      flip glUniform1i 2 =<< (withCString "ego"  $ glGetUniformLocation prog)
      flip glUniform1i 3 =<< (withCString "id"   $ glGetUniformLocation prog)
      flip glUniform1i 4 =<< (withCString "score"$ glGetUniformLocation prog)
      flip glUniform1i 5 =<< (withCString "mask" $ glGetUniformLocation prog)
      (\l -> glUniform2i l width height) =<< (withCString "size" $ glGetUniformLocation prog)
      vao <- with 0 $ \p -> glGenVertexArrays 1 p >> peek p
      glBindVertexArray vao

      frag <- glCreateShader GL_FRAGMENT_SHADER
      withCString (unlines
        [ "#version 330 core"
        , "in float hue;"
        , "layout(location = 0) out vec4 cout;"
        , "// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl"
        , "vec3 hsv2rgb(vec3 c)"
        , "{"
        , "    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);"
        , "    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);"
        , "    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);"
        , "}"
        , "void main() {"
        , "  cout = vec4(1.0);//vec4(hsv2rgb(vec3(hue, 1.0, 1.0)), 1.0);\n"
        , "}"
        ]) $ \s -> with s $ \string -> glShaderSource frag 1 string nullPtr
      glCompileShader frag
      vert <- glCreateShader GL_VERTEX_SHADER
      withCString (unlines
        [ "#version 330 core"
        , "layout(location = 0) in vec4 v;"
        , "out float hue;"
        , "void main() {"
        , "  hue = -log(v.z) / (20.0 * log(10.0));"
        , "  int n = int(round(abs(v.w))) - 1;"
        , "  float s = float((n % 30) / 5 * 5);"
        , "  float t = float(n / 30);"
        , "  vec2 p;"
        , "  p.x = float(n % 5);"
        , "  p.y = float(gl_VertexID % 2);"
        , "  if ((gl_VertexID % 2) == 1 && (n % 5) == 4) { p.x = 3.0 - p.x; }"
        , "  if (v.w < 0.0) { p.y = 1.0 - p.y; }"
        , "  gl_Position = vec4(  (1.0 + p.x + 0.25 * (v.x - 0.5) + s + 0.5) / 32.0 * (v.w > 0.0 ? -1.0 : 1.0) + (v.w > 0.0 ? 1.0 : -1.0)"
        , "                    , ((p.y + 0.25 * (v.y - 0.5) + t / 0.5) / 32.0 - 0.5) * (16.0/9.0), 0.0, 1.0);"
        , "}"
        ]) $ \s -> with s $ \string -> glShaderSource vert 1 string nullPtr
      glCompileShader vert
      prog2 <- glCreateProgram
      glAttachShader prog2 vert
      glAttachShader prog2 frag
      glLinkProgram prog2
      glUseProgram prog2
      vbo <- with 0 $ \p -> glGenBuffers 1 p >> peek p
      glBindBuffer GL_ARRAY_BUFFER vbo
      glBufferData GL_ARRAY_BUFFER (fromIntegral $ 1000 * 2 * 4 * sizeOf (0 :: Float)) nullPtr GL_STREAM_DRAW
      vao2 <- with 0 $ \p -> glGenVertexArrays 1 p >> peek p
      glBindVertexArray vao2
      glVertexAttribPointer 0 4 GL_FLOAT GL_FALSE 0 nullPtr
      glEnableVertexAttribArray 0

      frag <- glCreateShader GL_FRAGMENT_SHADER
      withCString (unlines
        [ "#version 330 core"
        , "uniform sampler2D mask;"
        , "uniform ivec2 size;"
        , "layout(location = 0) out vec4 cout;"
        , "void main() {"
        , "  cout = texture(mask, vec2(0.0, 1.0) + vec2(1.0, -1.0) * gl_FragCoord.xy / vec2(size));"
        , "}"
        ]) $ \s -> with s $ \string -> glShaderSource frag 1 string nullPtr
      glCompileShader frag
      vert <- glCreateShader GL_VERTEX_SHADER
      withCString (unlines
        [ "#version 330 core"
        , "void main() {"
        , "  if (gl_VertexID == 0) gl_Position = vec4(-1.0, -1.0, 0.0, 1.0);"
        , "  if (gl_VertexID == 1) gl_Position = vec4(-1.0,  1.0, 0.0, 1.0);"
        , "  if (gl_VertexID == 2) gl_Position = vec4( 1.0, -1.0, 0.0, 1.0);"
        , "  if (gl_VertexID == 3) gl_Position = vec4( 1.0,  1.0, 0.0, 1.0);"
        , "}"
        ]) $ \s -> with s $ \string -> glShaderSource vert 1 string nullPtr
      glCompileShader vert
      prog3 <- glCreateProgram
      glAttachShader prog3 vert
      glAttachShader prog3 frag
      glLinkProgram prog3
      glUseProgram prog3
      flip glUniform1i 6 =<< (withCString "mask" $ glGetUniformLocation prog3)
      (\l -> glUniform2i l width height) =<< (withCString "size" $ glGetUniformLocation prog3)

      framesR <- newIORef 0
      let showScoreboard 0 _ = return ()
          showScoreboard n count = do
            when record $ capturePPM width height
            modifyIORef framesR (+1)
            frames <- readIORef framesR
            when (record && frames == 9000) exitSuccess
            GLFW.swapBuffers window
            glClearColor 0 0 0 1
            glClear (GL_COLOR_BUFFER_BIT + GL_DEPTH_BUFFER_BIT)
            glUseProgram prog2
            glBindVertexArray vao2
            glDrawArrays GL_LINES 0 (count * 2)
            glUseProgram prog3
            glBindVertexArray vao
            glEnable GL_BLEND
            glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
            glDrawArrays GL_TRIANGLE_STRIP 0 4
            glDisable GL_BLEND
            GLFW.pollEvents
            stop <- GLFW.windowShouldClose window
            e <- glGetError
            when (e /= 0) $ hPutStrLn stderr $ "ERROR   " ++ show e
            unless stop $ showScoreboard (n - 1) count

          uploadScoreboard scoreboard = do
            withArray (concat
              [ map realToFrac [ x0, y0, z0, w, x1, y1, z1, w ]
              | ((LearningParameters x0 y0 z0, LearningParameters x1 y1 z1), w)
                <- (lefts  scoreboard `zip` [-1,-2..]) ++
                   (rights scoreboard `zip` [1..])
              ] :: [Float]) $
              glBufferSubData GL_ARRAY_BUFFER 0 (fromIntegral $ fromIntegral (length scoreboard) * 2 * 4 * sizeOf (0 :: Float)) . castPtr

          loop n m history scoreboard lastTime = do
            s@(ego, id, e0, i0, learnE, learnI) <- case m of
              Nothing -> random
              Just s -> pure s
            a <- async $ do gan (learnE, learnI) (ego, id)
            when record $ capturePPM width height
            modifyIORef framesR (+1)
            frames <- readIORef framesR
            when (record && frames == 9000) exitSuccess
            GLFW.swapBuffers window
            ((newEgo, newId), scores@(real:fake:poop:total:_)) <- wait a
            let s' = (newEgo, newId, e1, i1, learnE, learnI)
                history' = drop 3 history ++ [ real, fake, poop ]
                px :: S ('D3 143 1 3)
                px = S3D . LAS.fromList $ history'
                !e1 = reflE newEgo
                !i1 = reflI newId
                !de = e1 - e0
                !di = i1 - i0
            glActiveTexture GL_TEXTURE0
            V.unsafeWith (image  de) $ glTexSubImage2D GL_TEXTURE_2D 0 0 0 36 64 GL_RGB GL_UNSIGNED_BYTE . castPtr
            glActiveTexture GL_TEXTURE1
            V.unsafeWith (image  di) $ glTexSubImage2D GL_TEXTURE_2D 0 0 0 55 64 GL_RGB GL_UNSIGNED_BYTE . castPtr
            glActiveTexture GL_TEXTURE2
            V.unsafeWith (image  e1) $ glTexSubImage2D GL_TEXTURE_2D 0 0 0 36 64 GL_RGB GL_UNSIGNED_BYTE . castPtr
            glActiveTexture GL_TEXTURE3
            V.unsafeWith (image  i1) $ glTexSubImage2D GL_TEXTURE_2D 0 0 0 55 64 GL_RGB GL_UNSIGNED_BYTE . castPtr
            glActiveTexture GL_TEXTURE4
            V.unsafeWith (image' px) $ glTexSubImage2D GL_TEXTURE_2D 0 0 0 143 1 GL_RGB GL_UNSIGNED_BYTE . castPtr
            glClearColor 0 0 0 1
            glClear (GL_COLOR_BUFFER_BIT + GL_DEPTH_BUFFER_BIT)
            glUseProgram prog
            glBindVertexArray vao
            glDrawArrays GL_TRIANGLE_STRIP 0 4
            GLFW.pollEvents
            stop <- GLFW.windowShouldClose window
            e <- glGetError
            when (e /= 0) $ hPutStrLn stderr $ "ERROR   " ++ show e
            unless stop $ case () of
              _ | total > 4.5 -> do
                    Just now <- GLFW.getTime
                    hPutStrLn stderr $ "SUCCESS " ++ show n
                    hPutStrLn stderr $ "FPS " ++ show (fromIntegral n / (now - lastTime))
                    hFlush stderr
                    let scoreboard' = take 1024 $ Right (learnE, learnI) : scoreboard
                    uploadScoreboard scoreboard'
                    showScoreboard (15 * 5) (fromIntegral $ length scoreboard')
                    Just now <- GLFW.getTime
                    loop 0 Nothing (replicate (143 * 3) 0) scoreboard' now
                | n >= 999 -> do
                    Just now <- GLFW.getTime
                    hPutStrLn stderr $ "FAILURE " ++ show total
                    hPutStrLn stderr $ "FPS " ++ show (fromIntegral n / (now - lastTime))
                    hFlush stderr
                    let scoreboard' = take 1024 $ Left (learnE, learnI) : scoreboard
                    uploadScoreboard scoreboard'
                    showScoreboard (15 * 5) (fromIntegral $ length scoreboard')
                    Just now <- GLFW.getTime
                    loop 0 Nothing (replicate (143 * 3) 0) scoreboard' now
                | otherwise -> do
                    loop (n + 1) (Just s') history' scoreboard lastTime
      glClearColor 0 0 0 1
      glClear (GL_COLOR_BUFFER_BIT + GL_DEPTH_BUFFER_BIT)
      Just now <- GLFW.getTime
      loop 0 Nothing (replicate (143 * 3) 0) [] now

showN :: Show a => Char -> Int -> a -> String
showN c n = reverse . take n . (++ repeat c) . reverse . show

capturePPM :: Int -> Int -> IO ()
capturePPM vw vh = do
  let p6 = "P6\n" ++ show vw ++ " " ++ show vh ++ "\n255\n"
  allocaBytes (vw*vh*3) $ \ptr -> do
    glReadPixels 0 0 (fromIntegral vw) (fromIntegral vh) GL_RGB GL_UNSIGNED_BYTE (castPtr ptr)
    hPutStr stdout p6
    forM_ [0..vh-1] $ \y -> hPutBuf stdout (ptr `plusPtr` ((vh-1-y)*vw*3)) (vw*3)
    hFlush stdout

class Concrete a where
  reflect :: a -> Vector Double
  reify   :: [Double] -> Maybe (a, [Double])

instance Concrete (Pad a b c d) where
  reflect _ = V.empty
  reify xs = Just (Pad, xs)

instance Concrete (Crop a b c d) where
  reflect _ = V.empty
  reify xs = Just (Crop, xs)

instance Concrete (Pooling a b c d) where
  reflect _ = V.empty
  reify xs = Just (Pooling, xs)

instance Concrete Reshape where
  reflect _ = V.empty
  reify xs = Just (Reshape, xs)

instance Concrete Relu where
  reflect _ = V.empty
  reify xs = Just (Relu, xs)

instance Concrete Logit where
  reflect _ = V.empty
  reify xs = Just (Logit, xs)

instance ( KnownNat i
         , KnownNat o
         , KnownNat kx
         , KnownNat ky
         , KnownNat sx
         , KnownNat sy
         , KnownNat (kx * ky * i)
         ) => Concrete (Convolution i o kx ky sx sy) where
  reflect (Convolution ws _) = LA.flatten . extract $ ws
  reify xs = case splitAt n xs of
    (me, rest)
      | length me == n -> do
          ws <- create . LA.reshape o . LA.fromList $ me
          pure (Convolution ws (konst 0), rest)
    _ -> Nothing
    where
      n  = i * o * kx * ky
      i  = fromIntegral $ natVal (Proxy :: Proxy i)
      o  = fromIntegral $ natVal (Proxy :: Proxy o)
      kx = fromIntegral $ natVal (Proxy :: Proxy kx)
      ky = fromIntegral $ natVal (Proxy :: Proxy ky)

instance ( KnownNat i
         , KnownNat o
         , KnownNat kx
         , KnownNat ky
         , KnownNat sx
         , KnownNat sy
         , KnownNat (kx * ky * o)
         ) => Concrete (Deconvolution i o kx ky sx sy) where
  reflect (Deconvolution ws _) = LA.flatten . extract $ ws
  reify xs = case splitAt n xs of
    (me, rest)
      | length me == n -> do
          ws <- create . LA.reshape i . LA.fromList $ me
          pure (Deconvolution ws (konst 0), rest)
    _ -> Nothing
    where
      n  = i * o * kx * ky
      i  = fromIntegral $ natVal (Proxy :: Proxy i)
      o  = fromIntegral $ natVal (Proxy :: Proxy o)
      kx = fromIntegral $ natVal (Proxy :: Proxy kx)
      ky = fromIntegral $ natVal (Proxy :: Proxy ky)

instance (KnownNat i, KnownNat o) => Concrete (FullyConnected i o) where
  reflect (FullyConnected (FullyConnected' bs ws) _) = extract bs <> (LA.flatten . extract) ws
  reify xs = case splitAt o xs of
    (me, ys)
      | length me == o -> do
          bs <- create . LA.fromList $ me
          case splitAt (i * o) ys of
            (me, rest)
              | length me == i * o -> do
                  ws <- create . LA.reshape i . LA.fromList $ me
                  pure (FullyConnected (FullyConnected' bs ws) (FullyConnected' (konst 0) (konst 0)), rest)
            _ -> Nothing
    _ -> Nothing
    where
      i = fromIntegral $ natVal (Proxy :: Proxy i)
      o = fromIntegral $ natVal (Proxy :: Proxy o)

instance SingI i => Concrete (Network '[] '[i]) where
  reflect NNil = V.empty
  reify xs = Just (NNil, xs)

instance (SingI i, SingI o, Layer x i o, Concrete x, Concrete (Network xs (o ': rs))) => Concrete (Network (x ': xs) (i ': o ': rs)) where
  reflect (x :~> r) = reflect x <> reflect r
  reify xs = do
    (x, ys) <- reify xs
    (r, zs) <- reify ys
    pure (x :~> r, zs)
