#!/bin/bash
o=$1
mkdir -p "nnirror/${o}"
for i in 0 1 2 3 4 5 6 7 8 9
do
  shift
  j=$1
  rm "${i}.log"
  pushd "${j}"
  nnirror nets to images
  popd
  ln -s "${j}/nnirror.log" "${i}.log"
  convert "${j}.png" -rotate 270 -geometry 700x500 -quality 100 "nnirror/${o}/${i}-learn.jpg" &
  convert "${j}.png" -rotate 270                                "nnirror/${o}/${i}-learn.png" &
  pnmenlarge 108 < "${j}/ego.ppm" | pnmpad -white -left 144 -right 144 -top 144 -bottom 144 > "${j}/ego_large.ppm"
  pnmenlarge 108 < "${j}/id.ppm"  | pnmpad -white -left 144 -right 144 -top 144 -bottom 144 > "${j}/id_large.ppm"
  convert "${j}/ego_large.ppm" -geometry 700x500 -quality 100 "nnirror/${o}/${i}-ego.jpg" &
  convert "${j}/ego_large.ppm"                                "nnirror/${o}/${i}-ego.png" &
  convert "${j}/id_large.ppm"  -geometry 700x500 -quality 100 "nnirror/${o}/${i}-id.jpg" &
  convert "${j}/id_large.ppm"                                 "nnirror/${o}/${i}-id.png" &
  wait
  rm "${j}/ego_large.ppm" "${j}/id_large.ppm"
done
gnuplot < ../nnirror.gnuplot
convert "out.png" -geometry 700x500 -quality 100 "nnirror/${o}/metrics.jpg"
mv out.png "nnirror/${o}/metrics.png"
cat <<EOF > "nnirror/${o}/index.html"
<!DOCTYPE html>
<html><head><title>#${o} :: nnirror :: mathr</title><style>
body { width: 1500px; margin: auto; }
h1, h2, p { text-align: center; }
img { vertical-align: middle; }
a { text-decoration: none; }
a img { border: 1px solid; }
</style></head><body>
<h1>nnirror #${o}</h1>
<h2>metrics</h2><p><a href="metrics.png" title="metrics"><img src="metrics.jpg" alt="metrics" /></a></p>
EOF
for i in 0 1 2 3 4 5 6 7 8 9
do
  echo "<h2>${i}</h2><p><a href=\"${i}-learn.png\" title=\"${i} learn\"><img src=\"${i}-learn.jpg\" alt=\"${i} learn\" /></a>&nbsp;<a href=\"${i}-ego.png\" title=\"${i} ego\"><img src=\"${i}-ego.jpg\" alt=\"${i} ego\" /></a>&nbsp;<a href=\"${i}-id.png\" title=\"${i} id\"><img src=\"${i}-id.jpg\" alt=\"${i} id\" /></a></p>"
done >> "nnirror/${o}/index.html"
cat <<EOF >> "nnirror/${o}/index.html"
<hr />
<p><a href=".." title="nnirror">nnirror</a></p>
</body></html>
EOF
