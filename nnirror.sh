#!/bin/bash
nnirror1() {
  mkdir $1
  cd $1
  nnirror $1
  pnmtopng -compression 9 < nnirror.ppm > ../$1.png
  rm nnirror.ppm
  cd ..
}
export -f nnirror1
mkdir output &&
cd output &&
seq -w 0 999 |
parallel nnirror1 &&
for o in ???
do
  tail -n 1 $o/nnirror.log |
  ( read it a b c ; echo "$(../nnirror-score $a $b $c)" "$o" )
done |
sort -g -r |
cut -d\  -f 2 |
xargs -n 10 echo |
cat -n |
xargs -n 11 ../nnirror-plot.sh
cd nnirror
ls -1 |
sort -g |
while read n
do
  mv n $((n - 1))
done
rename s/^/0/ ?
cd ..
cd ..
