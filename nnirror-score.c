#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double u(double x)
{
  return -log(1 / x - 1);
}

int main(int argc, char **argv)
{
  if (argc != 4) return 1;
  double a = atof(argv[1]);
  double b = atof(argv[2]);
  double c = atof(argv[3]);
  double s = (2 * u(a) - u(b) - u(c)) / 4;
  printf("%f\n", s);
  return 0;
}
